# gitlab-ci-kube-manifest-template

This repository contains a template for GitLab CI. 

## KubeManifest.Base.gitlab-ci.yml

This is a GitLab template to apply a Kubernetes manifest via kubectl to a given namespace, allowing for DRYer CI YAML.

## Usage 

Define a `.gitlab-ci.yml` in your repository. 

```yaml
include:
  - remote: 'https://gitlab.com/chrisburke/gitlab-ci-kube-manifest-template/-/raw/main/KubeManifest.Base.gitlab-ci.yml'


your-job-name-here:
  extends: .kubectl-apply:manifest
  variables:
    NAMESPACE: <your-namespace>
    MANIFEST_FILE: <your-manifest-file.yaml>
```

The CI pipeline will be run when `<your-manifest-file.yaml>` is modified, which will `kubectl apply -f <your-manifest-file.yaml> -n <your-namespace>`

## `KUBECONFIG` Environment Variable

Export your `~/.kube/config` to an Environment Variable. To do this, you can base64 encode the file and add it to GitLab. An example one-liner on OS X for this is `cat ~/.kube/config | base64 | pbcopy` and pasting the content to a variable called `KUBECONFIG` in GitLab. 